package Part1;


public class AddRunnable implements Runnable{
	private int count;
	private MyRunnable m;
	private static final int DELAY = 1;
	public AddRunnable(MyRunnable m,int count) {
		this.m = m;
		this.count = count;
		
	}
	
	@Override
	public void run() {
		try{
			for(int i=1;i<=count;i++){
				m.add();
				Thread.sleep(DELAY);
			}
		}catch(InterruptedException e){
		}
	}
}
