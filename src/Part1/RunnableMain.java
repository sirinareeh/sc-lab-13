package Part1;

import java.util.LinkedList;


public class RunnableMain {

	public static void main(String[] args) {
		LinkedList<Integer> linklist = new LinkedList<Integer>();

		
		MyRunnable run = new MyRunnable();

		AddRunnable add = new AddRunnable(run, 5);
		RemoveRunnable remove = new RemoveRunnable(run,2);

		Thread a = new Thread(add);
		Thread b = new Thread(remove);
		
		a.start();
		b.start();

	}

}
