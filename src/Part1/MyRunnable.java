package Part1;

import java.util.LinkedList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MyRunnable {
	private Lock mngCountLock;
	private Condition mngCondition;
	private LinkedList<Integer> linkList;
	private int ele;
	
	public MyRunnable() {
		mngCountLock = new ReentrantLock();
		mngCondition = mngCountLock.newCondition();
		linkList = new LinkedList<Integer>();
	}
	
	public void add() {
		mngCountLock.lock();
		try {
			linkList.add(1);
			System.out.println("Element added into LinkedList");
			mngCondition.signalAll();
		}
		finally {
			mngCountLock.unlock();
		}
	}
	
	public void remove() throws InterruptedException {
		mngCountLock.lock();
		try {
			while(linkList.size()==0){
				mngCondition.await();

			}
			linkList.removeLast();
			System.out.println("Element removed into LinkedList");
				
		}
		finally {
			mngCountLock.unlock();
		}
	}
	public int getElement(int ele){
		return ele;
	}
}
