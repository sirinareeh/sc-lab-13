package Part2;


public class RemoveRunnable implements Runnable{
	private Consumer con;
	private int count;
	private String n;
	private static int DELAY = 1;
	
	public RemoveRunnable(Consumer con,int count,String n){
		this.con = con;
		this.count = count;
		this.n = n;
	}

	@Override
	public void run() {
		try{
			for(int i=1;i<=count;i++){
				con.remove(n);
				Thread.sleep(DELAY);
			}
		}catch(InterruptedException e){
			System.err.println("Error");
		}
	}
	
	
}
