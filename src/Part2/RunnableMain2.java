package Part2;


public class RunnableMain2 {

	public static void main(String[] args) {		
		Consumer con = new Consumer();
		AddRunnable add = new AddRunnable(con,100,"BOX");
		RemoveRunnable remove = new RemoveRunnable(con,100,"BOX");

		Thread a = new Thread(add);
		Thread b = new Thread(remove);
		
		a.start();
		b.start();
	}

}
